import csv 
import ordre

def readCSV(file):
    with open(file) as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        compteurLigne = 0
        for row in reader:
            ordre.ordonner(row, compteurLigne, 'test')
            compteurLigne+=1
            